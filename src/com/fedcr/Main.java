package com.fedcr;

import com.fedcr.service.MenuServiceImpl;

public class Main {

	public static void main(String[] args) {
		// Control program execution
		MenuServiceImpl thisMenu = new MenuServiceImpl();
		thisMenu.displayUserMenu();
	}

}
