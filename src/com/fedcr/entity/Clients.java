package com.fedcr.entity;

import java.util.concurrent.atomic.AtomicReference;

public class Clients {
    private Integer clientId;
    private String name;
    private Float totalAmount;
    private Float discountAmount;
    private String paymentType;

    public Clients() {

    }

    public Clients(Float key, float sumTotalAmount, float sumDiscountAmount) {
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Float getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Float discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
