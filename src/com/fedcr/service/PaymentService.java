package com.fedcr.service;

import com.fedcr.entity.Clients;
import com.fedcr.entity.PaymentPojo;

import java.io.IOException;
import java.util.List;

public interface PaymentService {
    PaymentPojo paymentMenu(String paymentType);
    float discountAmount(float totalAmount, String paymentType);
    void savePayment(Clients clients) throws IOException;
//    void readDataFromFile() throws IOException;
    List<Clients> readDataFromFiles() throws IOException;
    void displayListOfClients() throws IOException;
    void displayReportOfPayment() throws IOException;

}
