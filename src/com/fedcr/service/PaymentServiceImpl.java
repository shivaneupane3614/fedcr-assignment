package com.fedcr.service;

import com.fedcr.entity.Clients;
import com.fedcr.entity.PaymentPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class PaymentServiceImpl implements PaymentService {
    private float discountAmount;

    @Override
    public PaymentPojo paymentMenu(String paymentType){
        Scanner scanner = new Scanner(System.in);
        PaymentPojo paymentPojo = new PaymentPojo();
        System.out.println(paymentType);
        System.out.print("Name :");
        String name = scanner.next();
        System.out.print(name);
        System.out.println();
        System.out.print("Total Amount : ");
        float totalAmount = scanner.nextFloat();
        System.out.print(totalAmount);
        System.out.println("\n\n\n");

        paymentPojo.setName(name);
        paymentPojo.setTotalAmount(totalAmount);
        return paymentPojo;
    }

    @Override
    public float discountAmount(float totalAmount, String paymentType) {

        switch (paymentType) {
            case "Standard Payment":
                discountAmount = 0;
                break;
            case "Loyalty Payment":
                discountAmount = totalAmount / 10; //10% discount
                break;
            case "Employee Payment":
                discountAmount = totalAmount / 5; //20% discount
                break;
        }

        return discountAmount;
    }

    @Override
    public void savePayment(Clients clients) throws IOException {
        ArrayList<Clients> clientsList1 = new ArrayList<>();
        Gson gson = new GsonBuilder().create();
        File myFile = new File(System.getProperty("user.dir") + File.separator + "resource/clients.txt");
        if (!myFile.exists()) {
            System.out.println("File Not Exist ");
            myFile.createNewFile();
        }

        if(readDataFromFiles()!=null) {
            clientsList1 = readDataFromFiles();
            clientsList1.add(clients);
        }
        else {
            clientsList1.add(clients);
        }

        FileOutputStream fOut = new FileOutputStream(myFile);
        OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
        myOutWriter.append(gson.toJson(clientsList1));
        myOutWriter.close();
        fOut.close();
    }

    public ArrayList<Clients> readDataFromFiles() throws IOException {
//        File myFile = new File("/home/shiva/Desktop/clients.txt");
        File myFile = new File(System.getProperty("user.dir") + File.separator + "resource/clients.txt");
        if (!myFile.exists()) {
            System.out.println("File Not Exist ");
            myFile.createNewFile();
        }
        FileInputStream fIn = new FileInputStream(myFile);
        BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));


        String aDataRow = "";
        String aBuffer = ""; //Holds the text
        while ((aDataRow = myReader.readLine()) != null)
        {
            aBuffer += aDataRow ;
        }
        myReader.close();
        ArrayList<Clients> clientsList = jsonToClassConverter(aBuffer);
        return clientsList;
    }

    private ArrayList<Clients> jsonToClassConverter(String jsonText){
        Gson gson = new Gson();
        try{
            Type clientListType = new TypeToken<ArrayList<Clients>>(){}.getType();
            ArrayList<Clients> clientsArrayList = gson.fromJson(jsonText, clientListType);
            return clientsArrayList;
        }catch (Exception e){
            return null;
        }
    }

    public void displayListOfClients() throws IOException {
        ArrayList<Clients> clients = readDataFromFiles();
        Map<String, List<Clients>> clientsMap = new HashMap<>();
        System.out.println("\n\n\n\n");
        System.out.print("Client Id");
        System.out.print("\t\t\t");
        System.out.print("Name");
        System.out.print("\t\t");
        System.out.print("Total Amount");
        System.out.print("\t");
        System.out.print("Discount Amount");
        System.out.println();
        System.out.print("---------------------------------------------------------------");
        System.out.println();

        if (clients!=null) {
            clientsMap = clients.stream().collect(Collectors.groupingBy(Clients::getPaymentType));

            for (Map.Entry<String, List<Clients>> entry : clientsMap.entrySet()) {
                String k = entry.getKey();
                System.out.println(k);
                for (Clients clients1 : entry.getValue()) {
                    System.out.print(clients1.getClientId());
                    System.out.print("\t\t\t\t\t");
                    System.out.print(clients1.getName());
                    System.out.print("\t\t\t");

                    System.out.print(clients1.getTotalAmount());
                    System.out.print("\t\t\t\t");

                    System.out.print(clients1.getDiscountAmount());
                    System.out.println();
                }
            }
        }else
            System.out.print("\t\t\t\t\tNo data available");
        System.out.println("\n\n\n\n");
    }

    public void displayReportOfPayment() throws IOException {
        ArrayList<Clients> clients = readDataFromFiles();
        Map<String, List<Clients>> clientsMap = new HashMap<>();

        double sumStandardTotalAmount = 0;
        double sumStandardDiscountAmount = 0;
        double sumLoyaltyTotalAmount = 0;
        double sumLoyaltyDiscountAmount = 0;
        double sumEmployeeTotalAmount = 0;
        double sumEmployeeDiscountAmount = 0;
        double grandTotalSum = 0;
        double grandDiscountSum = 0;

        if (clients!=null){
            clientsMap = clients.stream().collect(Collectors.groupingBy(Clients::getPaymentType));

            if (clientsMap.get("Standard Payment")!=null){
                sumStandardTotalAmount = clientsMap.get("Standard Payment").stream().mapToDouble(i->i.getTotalAmount()).sum();
                sumStandardDiscountAmount = clientsMap.get("Standard Payment").stream().mapToDouble(i->i.getDiscountAmount()).sum();
            }
            if (clientsMap.get("Loyalty Payment")!=null){
                sumLoyaltyTotalAmount = clientsMap.get("Loyalty Payment").stream().mapToDouble(i->i.getTotalAmount()).sum();
                sumLoyaltyDiscountAmount = clientsMap.get("Loyalty Payment").stream().mapToDouble(i->i.getDiscountAmount()).sum();
            }
            if (clientsMap.get("Employee Payment")!=null){
                sumEmployeeTotalAmount = clientsMap.get("Employee Payment").stream().mapToDouble(i->i.getTotalAmount()).sum();
                sumEmployeeDiscountAmount = clientsMap.get("Employee Payment").stream().mapToDouble(i->i.getDiscountAmount()).sum();
            }
            grandTotalSum = sumStandardTotalAmount + sumLoyaltyTotalAmount + sumEmployeeTotalAmount;
            grandDiscountSum = sumStandardDiscountAmount + sumLoyaltyDiscountAmount + sumEmployeeDiscountAmount;
        }

        System.out.println("-------------------------------------------------------------------");
        System.out.print("Type of Account");
        System.out.print("\t\t\t");
        System.out.print("Total Amount Received");
        System.out.print("\t\t");
        System.out.print("Discounts Given");
        System.out.println();
        System.out.print("-------------------------------------------------------------------");
        System.out.println();


        for (Map.Entry<String, List<Clients>> entry : clientsMap.entrySet()) {
            String k = entry.getKey();
            System.out.print(k);
            if (k.equals("Standard Payment")){
                System.out.print("\t\t\t\t\t");
                System.out.print(sumStandardTotalAmount);
                System.out.print("\t\t\t\t\t");
                System.out.print(sumStandardDiscountAmount);
            }else if (k.equals("Loyalty Payment")){
                System.out.print("\t\t\t\t\t\t");
                System.out.print(sumLoyaltyTotalAmount);
                System.out.print("\t\t\t\t\t");
                System.out.print(sumLoyaltyDiscountAmount);
            }else if (k.equals("Employee Payment")){
                System.out.print("\t\t\t\t\t");
                System.out.print(sumEmployeeTotalAmount);
                System.out.print("\t\t\t\t\t");
                System.out.print(sumEmployeeDiscountAmount);
            }
            System.out.println();
        }
        if (clients==null){
            System.out.println("\t\t\t\t\t\tNo data available");
        }


        System.out.print("-------------------------------------------------------------------");
        System.out.println();
        System.out.print("Total");
        System.out.print("\t\t\t\t\t\t\t\t");
        System.out.print(grandTotalSum);
        System.out.print("\t\t\t\t\t");
        System.out.print(grandDiscountSum);
        System.out.println();
        System.out.print("-------------------------------------------------------------------");
        System.out.println();
        System.out.println("\n\n\n");


    }

}
