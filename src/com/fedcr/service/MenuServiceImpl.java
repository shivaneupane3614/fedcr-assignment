package com.fedcr.service;

import com.fedcr.entity.Clients;
import com.fedcr.entity.PaymentPojo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MenuServiceImpl implements MenuService {

    private PaymentService paymentService=null;

    public boolean processMenuSelection(int menuSelection) throws IOException {
        // Use the input provided by the user to activate the appropriate code function

        boolean blnContinue = true;
        PaymentPojo paymentPojo;
        float discountAmount;
        Clients clients;

        switch (menuSelection){
            case 1:
                // call method to process standard payments here
                System.out.println("\n\n");
                System.out.println("--Processing Standard Payment--");
                clients = new Clients();
                paymentService = new PaymentServiceImpl();
                paymentPojo = paymentService.paymentMenu("Standard Payment");
                ArrayList<Clients> data = (ArrayList<Clients>) paymentService.readDataFromFiles();
                if (data==null || data.isEmpty()){
                    clients.setClientId(1);
                }

                else{
                    clients.setClientId(data.get(data.size() - 1).getClientId() + 1);
                }

                clients.setName(paymentPojo.getName());
                clients.setTotalAmount(paymentPojo.getTotalAmount());
                clients.setDiscountAmount((float) 0);
                clients.setPaymentType("Standard Payment");
                paymentService.savePayment(clients);

//			No discount
                break;
            case 2:
                // call method to process loyalty rewards payments here
                System.out.println("\n\n");
                System.out.println("--Processing Loyalty Payment--");
                clients = new Clients();
                paymentService = new PaymentServiceImpl();
                paymentPojo = paymentService.paymentMenu("Loyalty Payment");
                discountAmount = paymentService.discountAmount(paymentPojo.getTotalAmount(),"Loyalty Payment");
                ArrayList<Clients> data1 = (ArrayList<Clients>) paymentService.readDataFromFiles();
                if (data1==null || data1.isEmpty()){
                    clients.setClientId(1);
                }
                else{
                    clients.setClientId(data1.get(data1.size() - 1).getClientId() + 1);
                }

                clients.setName(paymentPojo.getName());
                clients.setTotalAmount(paymentPojo.getTotalAmount());
                clients.setDiscountAmount(discountAmount);
                clients.setPaymentType("Loyalty Payment");
                paymentService.savePayment(clients);
                break;
            case 3:
                // call method to process employee payments here
                System.out.println("\n\n--Processing Employee Payment--");
                clients = new Clients();
                paymentService = new PaymentServiceImpl();
                paymentPojo = paymentService.paymentMenu("Employee Payment");
                discountAmount = paymentService.discountAmount(paymentPojo.getTotalAmount(),"Employee Payment");

                System.out.println("Loyalty Discount :: " + discountAmount);
                ArrayList<Clients> data2 = (ArrayList<Clients>) paymentService.readDataFromFiles();
                if (data2==null || data2.isEmpty()){
                    System.out.println("If");
                    clients.setClientId(1);
                }

                else{
                    System.out.println("Else");
                    clients.setClientId(data2.get(data2.size() - 1).getClientId() + 1);
                }

                clients.setName(paymentPojo.getName());
                clients.setTotalAmount(paymentPojo.getTotalAmount());
                clients.setDiscountAmount(discountAmount);
                clients.setPaymentType("Employee Payment");
                paymentService.savePayment(clients);
                break;
            case 4:
                paymentService = new PaymentServiceImpl();
                // call method to  display list of clients here
                System.out.println("\n\n--List of Clients--");
                paymentService.displayListOfClients();
                break;
            case 5:
                paymentService = new PaymentServiceImpl();
                // call method to generate report to display payments received here
                System.out.println("\n\n");
                System.out.println("--Report to View Payments Received--");
                paymentService.displayReportOfPayment();
                break;
            case 6:
                System.out.println("\n\n-- Exiting Federation Car Rental Payment Tracking System --\n ....\n  -- Goodbye! --\n");
                blnContinue = false;
                break;
            default:
                // no valid selection mad e
                System.out.println("\n\nInvalid selection! A number between 1 and 6 was expected.\n");
        }
        return blnContinue;
    }

    public void displayUserMenu() {
        // Control the menu navigation.  Includes display of menu, acceptance and processing of user input and
        // exiting the menu based on the user's selections.
        boolean blnContinue = true;
        Scanner scanInput = null;

        header();

        try {		//try-catch-finally to ensure compatibility with all java versions.
            scanInput = new Scanner(System.in);
            while (blnContinue) {
                displayMenu();

                if (scanInput.hasNextInt()) {
//                    System.out.println("scanInput.nextInt() :: " + scanInput.nextInt());
                    blnContinue = processMenuSelection(scanInput.nextInt());
                }
                else {
                    scanInput.nextLine();
                    blnContinue = processMenuSelection(0);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
            // Generic error catch - this can be made more specific to the expected errors.
            System.out.println("An unexpected exception has occurred with input");
        }
        finally {
            // Close the scanner
            if (scanInput != null) {
                scanInput.close();
            }
        }

    }

    private void header() {
        // Display program header information
        System.out.println("Federation Car Rental Payment Tracking System");
        System.out.println("=============================================");
        System.out.println();
    }

    private void displayMenu() {
        // Display the menu for user to select which function to run
        System.out.println("Select an option from the menu below:");
        System.out.println("1. Standard Payment");
        System.out.println("2. Loyalty Payment");
        System.out.println("3. Employee Payment");
        System.out.println("4. Display List of Clients");
        System.out.println("5. Generate Report To View Payments Received");
        System.out.println("6. Exit");
        System.out.print("Enter your option: ");

    }
}
