package com.fedcr.service;

import java.io.IOException;

public interface MenuService {
    boolean processMenuSelection(int menuSelection) throws IOException;
    void displayUserMenu();
}
